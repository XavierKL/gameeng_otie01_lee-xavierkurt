using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSpawner : MonoBehaviour
{
   public GameObject[] wallPrefabs;
   public float spawnWait;
   public float spawnMostWait;
   public float spawnLeastWait;
   public int startWait;
   private int randWall;

   void Start()
   {
       StartCoroutine(Spawner());
   }

   void Update()
   {
       spawnWait = Random.Range (spawnLeastWait, spawnMostWait);
   }

   IEnumerator Spawner()
   {
       yield return new WaitForSeconds(startWait);

       while (true)
       {
           randWall = Random.Range (0, 5);

           Vector3 spawnPosition = new Vector3 (0, 1.2832f, 12.76f);
           Instantiate(wallPrefabs[randWall], spawnPosition, transform.rotation);

           yield return new WaitForSeconds(startWait);
       }
   } 
}
