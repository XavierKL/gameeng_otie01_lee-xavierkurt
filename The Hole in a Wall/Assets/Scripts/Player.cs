using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    public WallScript wall;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (wall.color == Color.Red){ 

            Destroy(gameObject);
            SceneManager.LoadScene("GameOverScene");
            Time.timeScale = 0;
        }

         if (wall.color == Color.Blue){ 

            Destroy(gameObject);
            SceneManager.LoadScene("GameOverScene");
            Time.timeScale = 0;
        }

         if (wall.color == Color.Green){ 

            Destroy(gameObject);
            SceneManager.LoadScene("GameOverScene");
            Time.timeScale = 0;
        }

         if (wall.color == Color.Yellow){ 

            Destroy(gameObject);
            SceneManager.LoadScene("GameOverScene");
            Time.timeScale = 0;
        }

         if (wall.color == Color.Grey){ 

            Destroy(gameObject);
            SceneManager.LoadScene("GameOverScene");
            Time.timeScale = 0;
        }
    }

    void OnCollisionEnter(Collision col){

        if(col.gameObject.tag == "Wall"){

            wall = col.gameObject.GetComponent<WallScript>();
        }

        
    }
}
