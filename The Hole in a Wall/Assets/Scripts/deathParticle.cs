using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathParticle : MonoBehaviour
{
    public static deathParticle instance;
    void Awake()
    {
        bool created = false;
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true; // which runs "else" statement exactly the next frame )))
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}