using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMove : MonoBehaviour
{
    private float movementSpeed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame

 
    void FixedUpdate()
    {
        movementSpeed = Random.Range(10, 15);
        transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
    }

    void OnCollsionEnter(Collision col){

        if(col.gameObject.tag == "Player"){

            Debug.Log("Player Hit");
        }
    }
}
