using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SwipeInputScript : MonoBehaviour
{

    public TouchAndSwipe swipe;
    public GameObject[] deathParticles;
    public GameObject currentWall;
    public GameObject ps;
    public WallScript wall;
    public TextMeshProUGUI score;
    int scoreNumber = 0;

    

    // Start is called before the first frame update
    void Start()
    {
        score.text = scoreNumber.ToString();
    }

    void OnTriggerEnter(Collider col){

        if (col.gameObject.tag == "Wall"){ // wall script should contain an enum, wall tag should be "wall"

            this.currentWall = col.gameObject;
            wall = col.gameObject.GetComponent<WallScript>();
            Debug.Log("Red wall is now the current wall");
        } 


    }

    // Update is called once per frame
    public void Update()
    {
        foreach (GameObject particles in deathParticles)
        {

        }

        if (swipe.SwipeLeft){

            if (wall.color == Color.Red){ 

                scoreNumber += 1;
                Destroy(this.currentWall);
                FindObjectOfType<AudioManager>().Play("WallBroken");
                GameObject ps = Instantiate(deathParticles[0], transform.position, Quaternion.identity);
                Destroy (ps, 0.5f);
                Debug.Log("Red Wall destroyed");
                score.text = scoreNumber.ToString();
            }
        }

        if (swipe.SwipeRight){

            if (wall.color == Color.Blue){

                scoreNumber += 1;
                Destroy(this.currentWall);
                FindObjectOfType<AudioManager>().Play("WallBroken");
                GameObject ps = Instantiate(deathParticles[1], transform.position, Quaternion.identity);
                Destroy (ps, 0.5f);
                Debug.Log("Blue Wall destroyed");
                score.text = scoreNumber.ToString();
            }
        }

        if (swipe.SwipeDown){

            if (wall.color == Color.Green){

                scoreNumber += 1;
                Destroy(this.currentWall);
                FindObjectOfType<AudioManager>().Play("WallBroken");
                GameObject ps = Instantiate(deathParticles[2], transform.position, Quaternion.identity);
                Destroy (ps, 0.5f);
                Debug.Log("Green Wall destroyed");
                score.text = scoreNumber.ToString();
            }
        }

        if (swipe.SwipeUp){

            if (wall.color == Color.Yellow){

                scoreNumber += 1;
                Destroy(this.currentWall);
                FindObjectOfType<AudioManager>().Play("WallBroken");
                GameObject ps = Instantiate(deathParticles[3], transform.position, Quaternion.identity);
                Destroy (ps, 0.5f);
                Debug.Log("Yellow Wall destroyed");
                score.text = scoreNumber.ToString();
            }
        }

        if (swipe.Tap){

            if (wall.color == Color.Grey){

                scoreNumber += 1;
                Destroy(this.currentWall);
                FindObjectOfType<AudioManager>().Play("WallBroken");
                GameObject ps = Instantiate(deathParticles[4], transform.position, Quaternion.identity);
                Destroy (ps, 0.5f);
                Debug.Log("Gray Wall destroyed");
                score.text = scoreNumber.ToString();
            }
        }

        if (scoreNumber > PlayerPrefs.GetInt("HighScore", 0)){


            PlayerPrefs.SetInt("HighScore", scoreNumber); 
        }

        score.text = scoreNumber.ToString();
    }
}
