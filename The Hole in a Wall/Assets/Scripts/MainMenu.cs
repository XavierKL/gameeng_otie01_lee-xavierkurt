using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour
{

    public TextMeshProUGUI scoreText;
    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "HIGH SCORE: " + PlayerPrefs.GetInt("HighScore", 0).ToString(); 
    }

    public void StartGame(){

        SceneManager.LoadScene("GameScene");
        Time.timeScale = 1;
    }

    public void GoBackToMenu(){

        SceneManager.LoadScene("MenuScene");
        Time.timeScale = 0;
    }

    public void ShowHighScore(){

        scoreText.text = "HIGH SCORE: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    public void ResetScore(){

        PlayerPrefs.DeleteKey("HighScore");
        scoreText.text = "HIGH SCORE: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
    }
}
